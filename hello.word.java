import java.util.*;

public class HelloWorld {
  public static void main(String[] args) {
    System.out.println("Hello World");
    
    int numeroInteiro = 10;
    float numeroDecimal = 2.5f;
    String texto = "Hahaha";
    Boolean amiga = false;
    String nome = "Rafael";
    int idade = 33;
    
    System.out.println(texto);
    System.out.println(numeroDecimal);
    System.out.println(numeroInteiro);
    System.out.println(amiga);
    
    System.out.println("O aluno " + nome + " Idade "+idade);
    
    
    int a = 5;
    int b = 8;
    int soma = a + b;
    int subtracao = b - a;
    int multiplicacao = a * b;
    int divisao = a/b;
    int resto = 5 % 2;
    
    System.out.println(soma);
    System.out.println(subtracao);
    System.out.println(multiplicacao);
    System.out.println(divisao);
    System.out.println(resto);
    
    String palavra = "Rafael Paulino";
    System.out.println(palavra.length());
    
    int[] listaNumeros = {5,10,15};
    System.out.println(listaNumeros[2]);
    
    List listaObjetos = new ArrayList();
    
    listaObjetos.add(10);
    listaObjetos.add(100);
    listaObjetos.add(500);
    
    listaObjetos.remove(0);
    
    System.out.println(listaObjetos);
    System.out.println(listaObjetos.get(0));
    
    Random geradorAleatorio = new Random();
    int numeroRandom = geradorAleatorio.nextInt(10);
    System.out.println(numeroRandom);
    
    for (int i=0; i<10; i++) {
	System.out.println(i);
    }
    
    int x = 0;
    
    while (x<=5) {
    	System.out.println(x);
    	x++;
    }
    
    List<String> familiaFelpudo = new ArrayList<String>();
    
    familiaFelpudo.add("Cebolinha");
    familiaFelpudo.add("Monica");
    familiaFelpudo.add("Cascao");
    familiaFelpudo.add("Magali");
    
    
    for (String nomeMembro:familiaFelpudo) {
    	System.out.println(nomeMembro);
    }
    
  }
}
